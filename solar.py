#!/usr/bin/env python3
import requests

ASTRONOMYAPI_ID = "a52bf9da-2354-4faa-ab80-837ed9232889"
ASTRONOMYAPI_SECRET = "a3a1deb7b2425dc3337fdc3327b3bc0050484705ef18a69d8564e06c8ccae79238bcf159e763054633635e36a64e690092176f0e7bb17131d97c73c5e54a7bee4c8797ee1564757e37219a2696d757b1b2e80b21d60fa56cebaefb7bb4e6400cbc77afcfb34126e3d0b25a054540762f"
payload = {
    "latitude": "42.898723",
    "longitude": "-70.812940",
    "elevation": "50",
    "from_date": "2021-04-10",
    "to_date": "2021-04-11",
    "time": "08:00:00",
}


def get_observer_location():

    response = requests.get("http://ip-api.com/json/").json()
    lat1 = response["lat"]
    lon1 = response["lon"]

    return lat1, lon1


def get_sun_position(latitude, longitude):

    payload["latitude"] = latitude
    payload["longitude"] = longitude

    url = "https://api.astronomyapi.com/api/v2/bodies/positions/sun"
    with requests.Session() as session:
        session.auth = (ASTRONOMYAPI_ID, ASTRONOMYAPI_SECRET)

        response = session.get(url, params=payload).json()

        azimuth = response["data"]["table"]["rows"][0]["cells"][0]["position"][
            "horizonal"
        ]["azimuth"]["degrees"]
        altitude = response["data"]["table"]["rows"][0]["cells"][0]["position"][
            "horizonal"
        ]["altitude"]["degrees"]

    return azimuth, altitude


def print_position(azimuth, altitude):

    print(
        f"The Sun is currently at: {azimuth} degrees azimuth {altitude} degrees altitude"
    )


if __name__ == "__main__":
    latitude, longitude = get_observer_location()
    azimuth, altitude = get_sun_position(latitude, longitude)
    print_position(azimuth, altitude)
